"""Problem Set 6: PCA, Boosting, Haar Features, Viola-Jones."""
import numpy as np
import cv2
import os

from helper_classes import WeakClassifier, VJ_Classifier


# assignment code
def load_images(folder, size=(32, 32)):
    """Load images to workspace.

    Args:
        folder (String): path to folder with images.
        size   ([int]): new image sizes

    Returns:
        tuple: two-element tuple containing:
            X (numpy.array): data matrix of flatten images
                             (row:observations, col:features) (float).
            y (numpy.array): 1D array of labels (int).
    """
    images_files = [f for f in os.listdir(folder) if f.endswith(".png")]
    y = np.zeros((len(images_files), ), dtype=np.int)
    for i, img in enumerate(images_files):
        y[i] = int(img[7:9])

    # import ipdb; ipdb.set_trace()
    num_cols = size[0] * size[1]
    size_t = tuple(size)
    X = np.zeros((len(images_files), num_cols))
    # resized_images = [cv2.resize(img, size) for img in images_files]
    # flatten_images = [img.flatten() for img in images_files]
    imgs = [np.array(cv2.imread(os.path.join(folder, f), 0))
            for f in images_files]
    for x in imgs:
        new_img = cv2.resize(x, size_t)
        assert new_img.shape == size_t
        resized_images = new_img
    resized_images = [cv2.resize(x, size_t) for x in imgs]

    # import ipdb; ipdb.set_trace()
    for i, img in enumerate(resized_images):
        X[i,:] = img.flatten()

    return X, y


def split_dataset(X, y, p):
    """Split dataset into training and test sets.

    Let M be the number of images in X, select N random images that will
    compose the training data (see np.random.permutation). The images that
    were not selected (M - N) will be part of the test data. Record the labels
    accordingly.

    Args:
        X (numpy.array): 2D dataset.
        y (numpy.array): 1D array of labels (int).
        p (float): Decimal value that determines the percentage of the data
                   that will be the training data.

    Returns:
        tuple: Four-element tuple containing:
            Xtrain (numpy.array): Training data 2D array.
            ytrain (numpy.array): Training data labels.
            Xtest (numpy.array): Test data test 2D array.
            ytest (numpy.array): Test data labels.
    """
    M, d = X.shape
    assert M == len(y)

    N = int(np.ceil(M * p))
    T = M - N
    assert N + T == M

    random_indices = np.random.permutation(M)
    train_indices = random_indices[:N]
    test_indices = random_indices[N:]
    Xtrain = X[train_indices, :]
    Xtest = X[test_indices, :]
    ytrain = y[train_indices]
    ytest = y[test_indices]

    assert train_indices.shape == (N, )
    assert test_indices.shape == (T, )

    return Xtrain, ytrain, Xtest, ytest


def get_mean_face(x):
    """Return the mean face.

    Calculate the mean for each column.

    Args:
        x (numpy.array): array of flattened images.

    Returns:
        numpy.array: Mean face.
    """
    rows, cols = x.shape
    x_mean = np.zeros((cols, ), np.float64)

    for col in range(cols):
        x_mean[col] = np.mean(x[:, col])

    return x_mean


def pca(X, k):
    """PCA Reduction method.

    Return the top k eigenvectors and eigenvalues using the covariance array
    obtained from X.


    Args:
        X (numpy.array): 2D data array of flatten images (row:observations,
                         col:features) (float).
        k (int): new dimension space

    Returns:
        tuple: two-element tuple containing
            eigenvectors (numpy.array): 2D array with the top k eigenvectors.
            eigenvalues (numpy.array): array with the top k eigenvalues.
    """
    # M, d
    rows, cols = X.shape
    M, d = X.shape
    x_mean = get_mean_face(X)

    # d == number of pixels
    assert cols == len(x_mean)

    # Subtract x_mean from every row in X
    delta = np.zeros_like(X, dtype=np.float64)
    for i, img in enumerate(X):
        delta[i] = img - x_mean

    A = X - x_mean

    assert np.array_equal(A, delta)

    # To be consistent with lecture notation
    A = A.T
    A = (1./np.sqrt(M)) * A

    # sigma = np.dot(A.T, A) / M
    # sigma = (1./ M) * np.dot(X_sub.T, X_sub)
    sigma = np.dot(A.T, A)

    w, v = np.linalg.eigh(sigma)
    derived_eigen_vals = M * w
    derived_eigen_vectors = M * np.dot(A, v)

    derived_eigen_vals = derived_eigen_vals[:(-k-1):-1]
    derived_eigen_vectors = derived_eigen_vectors[:,:(-k-1):-1]

    assert derived_eigen_vals.shape == (k, )
    assert derived_eigen_vectors.shape == (d, k)

    return derived_eigen_vectors, derived_eigen_vals


class Boosting:
    """Boosting classifier.

    Args:
        X (numpy.array): Data array of flattened images
                         (row:observations, col:features) (float).
        y (numpy.array): Labels array of shape (observations, ).
        num_iterations (int): number of iterations
                              (ie number of weak classifiers).

    Attributes:
        Xtrain (numpy.array): Array of flattened images (float32).
        ytrain (numpy.array): Labels array (float32).
        num_iterations (int): Number of iterations for the boosting loop.
        weakClassifiers (list): List of weak classifiers appended in each
                               iteration.
        alphas (list): List of alpha values, one for each classifier.
        num_obs (int): Number of observations.
        weights (numpy.array): Array of normalized weights, one for each
                               observation.
        eps (float): Error threshold value to indicate whether to update
                     the current weights or stop training.
    """

    def __init__(self, X, y, num_iterations):
        self.Xtrain = np.float32(X)
        self.ytrain = np.float32(y)
        self.num_iterations = num_iterations
        self.weakClassifiers = []
        self.alphas = []
        self.num_obs = X.shape[0]  # M
        self.weights = np.array([1.0 / self.num_obs] * self.num_obs)  # uniform weights
        self.eps = 0.0001

    def train(self):
        """Implement the for loop shown in the problem set instructions."""

        for j in range(self.num_iterations):
            # Renormalize the weights so they sum to 1
            self.weights = self.weights / np.sum(self.weights)

            # Instantiate the Weak Classifier h with the training data
            # and labels.
            # TODO Include threshold here?
            wk_clf = WeakClassifier(self.Xtrain, self.ytrain, self.weights)
            wk_clf.train()
            self.weakClassifiers.append(wk_clf)
            wk_results = [wk_clf.predict(x) for x in self.Xtrain]

            # Find epsilon_j
            epsilon_j = 0.0
            for i in range(self.num_obs):
                if wk_results[i] != self.ytrain[i]:
                    epsilon_j += self.weights[i]

            # Calculate alpha_j
            alpha_j = 0.5 * np.log((1.0 - epsilon_j) / epsilon_j)
            self.alphas.append(alpha_j)

            # If epsilon_j is greator than a small threshold
            if epsilon_j > self.eps:
                # Update the weights
                for i in range(self.num_obs):
                    self.weights[i] = self.weights[i] \
                            * np.exp(-self.ytrain[i] * alpha_j * wk_results[i])
            else:
                break

        assert len(self.weakClassifiers) == len(self.alphas)

    def evaluate(self):
        """Return the number of correct and incorrect predictions.

        Use the training data (self.Xtrain) to obtain predictions. Compare
        them with the training labels (self.ytrain) and return how many
        where correct and incorrect.

        Returns:
            tuple: two-element tuple containing:
                correct (int): Number of correct predictions.
                incorrect (int): Number of incorrect predictions.
        """
        y_pred = self.predict(self.Xtrain)
        good, bad = 0, 0

        for i in range(len(y_pred)):
            if y_pred[i] == self.ytrain[i]:
                good += 1
            else:
                bad += 1

        return good, bad

    def predict(self, X):
        """Return predictions for a given array of observations.

        Use the alpha values stored in self.aphas and the weak classifiers
        stored in self.weakClassifiers.

        Args:
            X (numpy.array): Array of flattened images (observations).

        Returns:
            numpy.array: Predictions, one for each row in X.
        """
        assert len(self.weakClassifiers) == len(self.alphas)

        X_num_obs, X_d = X.shape
        y_pred = np.zeros((X_num_obs, ), np.int)

        for i in range(X_num_obs):
            x_observation = X[i]
            y_d = 0.0
            for j in range(len(self.alphas)):
                wk_results = self.weakClassifiers[j].predict(x_observation)
                y_d += self.alphas[j] * wk_results
            y_pred[i] = np.sign(y_d)

        return y_pred

class HaarFeature:
    """Haar-like features.

    Args:
        feat_type (tuple): Feature type {(2, 1), (1, 2), (3, 1), (2, 2)}.
        position (tuple): (row, col) position of the feature's top left corner.
        size (tuple): Feature's (height, width)

    Attributes:
        feat_type (tuple): Feature type.
        position (tuple): Feature's top left corner.
        size (tuple): Feature's width and height.
    """

    def __init__(self, feat_type, position, size):
        self.feat_type = feat_type
        self.position = position
        self.size = size

    def _create_two_horizontal_feature(self, shape):
        """Create a feature of type (2, 1).

        Use int division to obtain half the height.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        image = np.zeros(shape, dtype=np.uint8)
        feature = np.full(self.size, 255)
        rows, cols = self.size

        middle_row = rows // 2
        feature[middle_row:, :] = 126

        s_row, s_col = self.size
        p_row, p_col = self.position
        image[p_row:p_row + rows, p_col:p_col + cols] = feature

        return image

    def _create_two_vertical_feature(self, shape):
        """Create a feature of type (1, 2).

        Use int division to obtain half the width.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        image = np.zeros(shape, dtype=np.uint8)
        feature = np.full(self.size, 255)
        rows, cols = self.size

        middle_col = cols // 2
        feature[:, middle_col:] = 126

        p_row, p_col = self.position
        image[p_row:p_row + rows, p_col:p_col + cols] = feature

        return image

    def _create_three_horizontal_feature(self, shape):
        """Create a feature of type (3, 1).

        Use int division to obtain a third of the height.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        image = np.zeros(shape, dtype=np.uint8)
        feature = np.full(self.size, 255)
        rows, cols = self.size

        middle_row = rows // 3
        feature[middle_row:(2*middle_row), :] = 126

        p_row, p_col = self.position
        image[p_row:p_row + rows, p_col:p_col + cols] = feature

        return image

    def _create_three_vertical_feature(self, shape):
        """Create a feature of type (1, 3).

        Use int division to obtain a third of the width.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        image = np.zeros(shape, dtype=np.uint8)
        feature = np.full(self.size, 255)
        rows, cols = self.size

        middle_col = cols // 3
        feature[:, middle_col:(2*middle_col)] = 126

        p_row, p_col = self.position
        image[p_row:p_row + rows, p_col:p_col + cols] = feature

        return image

    def _create_four_square_feature(self, shape):
        """Create a feature of type (2, 2).

        Use int division to obtain half the width and half the height.

        Args:
            shape (tuple):  Array numpy-style shape (rows, cols).

        Returns:
            numpy.array: Image containing a Haar feature. (uint8).
        """
        image = np.zeros(shape, dtype=np.uint8)
        feature = np.full(self.size, 255)
        rows, cols = self.size

        middle_row = rows // 2
        middle_col = cols // 2
        feature[:middle_row, :middle_col] = 126
        feature[middle_row:, middle_col:] = 126

        p_row, p_col = self.position
        image[p_row:p_row + rows, p_col:p_col + cols] = feature

        return image

    def preview(self, shape=(24, 24), filename=None):
        """Return an image with a Haar-like feature of a given type.

        Function that calls feature drawing methods. Each method should
        create an 2D zeros array. Each feature is made of a white area (255)
        and a gray area (126).

        The drawing methods use the class attributes position and size.
        Keep in mind these are in (row, col) and (height, width) format.

        Args:
            shape (tuple): Array numpy-style shape (rows, cols).
                           Defaults to (24, 24).

        Returns:
            numpy.array: Array containing a Haar feature (float or uint8).
        """

        if self.feat_type == (2, 1):  # two_horizontal
            X = self._create_two_horizontal_feature(shape)

        if self.feat_type == (1, 2):  # two_vertical
            X = self._create_two_vertical_feature(shape)

        if self.feat_type == (3, 1):  # three_horizontal
            X = self._create_three_horizontal_feature(shape)

        if self.feat_type == (1, 3):  # three_vertical
            X = self._create_three_vertical_feature(shape)

        if self.feat_type == (2, 2):  # four_square
            X = self._create_four_square_feature(shape)

        if filename is None:
            cv2.imwrite("output/{}_feature.png".format(self.feat_type), X)

        else:
            cv2.imwrite("output/{}.png".format(filename), X)

        return X

    def evaluate(self, ii):
        """Evaluates a feature's score on a given integral image.

        Calculate the score of a feature defined by the self.feat_type.
        Using the integral image and the sum / subtraction of rectangles to
        obtain a feature's value. Add the feature's white area value and
        subtract the gray area.

        For example, on a feature of type (2, 1):
        score = sum of pixels in the white area - sum of pixels in the gray area

        Keep in mind you will need to use the rectangle sum / subtraction
        method and not numpy.sum(). This will make this process faster and
        will be useful in the ViolaJones algorithm.

        Args:
            ii (numpy.array): Integral Image.

        Returns:
            float: Score value.
        """
        # Pad image with zeros on top and left border
        ii_rows, ii_cols = ii.shape
        ii_dtype = ii.dtype
        zeros_row = np.zeros((1, ii_cols), dtype=ii_dtype)
        zeros_col = np.zeros(((ii_rows + 1), 1), dtype=ii_dtype)
        ii_row = np.concatenate((zeros_row, ii), axis=0)
        ii_padded = np.concatenate((zeros_col, ii_row), axis=1)

        p_row, p_col = self.position
        # New position after padding
        p_row, p_col = p_row + 1, p_col + 1
        rows, cols = self.size

        row_max, col_max = (p_row + rows - 1), (p_col + cols - 1)
        # For two and four slices
        middle_row = p_row + ((rows - 1) // 2)
        middle_col = p_col + ((cols - 1) // 2)

        # For three slices
        middle_row_first = p_row + ((rows - 1) // 3)
        middle_row_second = (middle_row_first + 1) + ((rows - 1) // 3)
        middle_col_first = p_col + ((cols - 1) // 3)
        middle_col_second = (middle_col_first + 1) + ((cols - 1) // 3)

        if self.feat_type == (2, 1):
            D = ii_padded[middle_row, col_max]
            B = ii_padded[p_row - 1, col_max]
            C = ii_padded[middle_row, p_col - 1]
            A = ii_padded[p_row - 1, p_col - 1]
            top_row_white = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, col_max]
            B = ii_padded[middle_row, col_max]
            C = ii_padded[row_max, p_col - 1]
            A = ii_padded[middle_row, p_col - 1]
            bottom_row_gray = int(D) - int(B) - int(C) + int(A)

            # print "TopWhite: {} BottomGray: {}".format(
            #         top_row_white, bottom_row_gray)
            score_value = int(top_row_white) - int(bottom_row_gray)

        if self.feat_type == (1, 2):
            D = ii_padded[row_max, middle_col]
            B = ii_padded[p_row - 1, middle_col]
            C = ii_padded[row_max, p_col - 1]
            A = ii_padded[p_row - 1, p_col - 1]
            left_col_white = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, col_max]
            B = ii_padded[p_row - 1, col_max]
            C = ii_padded[row_max, middle_col]
            A = ii_padded[p_row - 1, middle_col]
            right_col_gray = int(D) - int(B) - int(C) + int(A)

            # print "LeftWhite: {} RightGray: {}".format(
            #         left_col_white, right_col_gray)
            score_value = int(left_col_white) - int(right_col_gray)

        if self.feat_type == (3, 1):
            D = ii_padded[middle_row_first, col_max]
            B = ii_padded[p_row - 1, col_max]
            C = ii_padded[middle_row_first, p_col - 1]
            A = ii_padded[p_row - 1, p_col - 1]
            top_row_white = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[middle_row_second, col_max]
            B = ii_padded[middle_row_first, col_max]
            C = ii_padded[middle_row_second, p_col - 1]
            A = ii_padded[middle_row_first, p_col - 1]
            middle_row_gray = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, col_max]
            B = ii_padded[middle_row_second, col_max]
            C = ii_padded[row_max, p_col - 1]
            A = ii_padded[middle_row_second, p_col - 1]
            bottom_row_white = int(D) - int(B) - int(C) + int(A)

            # print "TopWhite: {} MiddleGrey: {} BottomWhite: {}".format(
            #         top_row_white, middle_row_gray, bottom_row_white)
            score_value = int(top_row_white) - int(middle_row_gray) \
                           + int(bottom_row_white)

        if self.feat_type == (1, 3):
            D = ii_padded[row_max, middle_col_first]
            B = ii_padded[p_row - 1, middle_col_first]
            C = ii_padded[row_max, p_col - 1]
            A = ii_padded[p_row - 1, p_col - 1]
            left_col_white = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, middle_col_second]
            B = ii_padded[p_row - 1, middle_col_second]
            C = ii_padded[row_max, middle_col_first]
            A = ii_padded[p_row - 1, middle_col_first]
            middle_col_gray = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, col_max]
            B = ii_padded[p_row - 1, col_max]
            C = ii_padded[row_max, middle_col_second]
            A = ii_padded[p_row - 1, middle_col_second]
            right_col_white = int(D) - int(B) - int(C) + int(A)

            # print "LeftWhite: {} MiddleGrey: {} RightWhite: {}".format(
            #         left_col_white, middle_col_gray, right_col_white)
            score_value = int(left_col_white) - int(middle_col_gray) \
                           + int(right_col_white)

        if self.feat_type == (2, 2):
            D = ii_padded[middle_row, middle_col]
            B = ii_padded[p_row - 1, middle_col]
            C = ii_padded[middle_row, p_col - 1]
            A = ii_padded[p_row - 1, p_col - 1]
            top_left_gray = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[middle_row, col_max]
            B = ii_padded[p_row - 1, col_max]
            C = ii_padded[middle_row, middle_col]
            A = ii_padded[p_row - 1, middle_col]
            top_right_white = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, middle_col]
            B = ii_padded[middle_row, middle_col]
            C = ii_padded[row_max, p_col - 1]
            A = ii_padded[middle_row, p_col - 1]
            bottom_left_white = int(D) - int(B) - int(C) + int(A)

            D = ii_padded[row_max, col_max]
            B = ii_padded[middle_row, col_max]
            C = ii_padded[row_max, middle_col]
            A = ii_padded[middle_row, middle_col]
            bottom_right_gray = int(D) - int(B) - int(C) + int(A)

            # print "TopLeftGray: {} TopRightWhite: {} \
            #         BottomLeftWhite: {} BottomRightGray: {}".format(
                    # top_left_gray, top_right_white,
                    # bottom_left_white, bottom_right_gray)

            score_value = int(top_right_white) - int(top_left_gray) \
                           - int(bottom_right_gray) + int(bottom_left_white)

        return score_value


def convert_images_to_integral_images(images):
    """Convert a list of grayscale images to integral images.

    Args:
        images (list): List of grayscale images (uint8 or float).

    Returns:
        (list): List of integral images.
    """
    integral_images = []

    for image in images:
        # MDT might need to convert to np.int64 or np.float64
        row_sum = np.cumsum(image, axis=0)
        i_image = np.cumsum(row_sum, axis=1)
        integral_images.append(i_image)

    return integral_images


class ViolaJones:
    """Viola Jones face detection method

    Args:
        pos (list): List of positive images.
        neg (list): List of negative images.
        integral_images (list): List of integral images.

    Attributes:
        haarFeatures (list): List of haarFeature objects.
        integralImages (list): List of integral images.
        classifiers (list): List of weak classifiers (VJ_Classifier).
        alphas (list): Alpha values, one for each weak classifier.
        posImages (list): List of positive images.
        negImages (list): List of negative images.
        labels (numpy.array): Positive and negative labels.
    """
    def __init__(self, pos, neg, integral_images):
        self.haarFeatures = []
        self.integralImages = integral_images
        self.classifiers = []
        self.alphas = []
        self.posImages = pos
        self.negImages = neg
        self.labels = np.hstack((np.ones(len(pos)), -1*np.ones(len(neg))))

    def createHaarFeatures(self):
        # Let's take detector resolution of 24x24 like in the paper
        FeatureTypes = {"two_horizontal": (2, 1),
                        "two_vertical": (1, 2),
                        "three_horizontal": (3, 1),
                        "three_vertical": (1, 3),
                        "four_square": (2, 2)}

        haarFeatures = []
        for _, feat_type in FeatureTypes.iteritems():
            for sizei in range(feat_type[0], 24 + 1, feat_type[0]):
                for sizej in range(feat_type[1], 24 + 1, feat_type[1]):
                    for posi in range(0, 24 - sizei + 1, 4):
                        for posj in range(0, 24 - sizej + 1, 4):
                            haarFeatures.append(
                                HaarFeature(feat_type, [posi, posj],
                                            [sizei-1, sizej-1]))
        self.haarFeatures = haarFeatures

    def train(self, num_classifiers):

        # Use this scores array to train a weak classifier using VJ_Classifier
        # in the for loop below.
        scores = np.zeros((len(self.integralImages), len(self.haarFeatures)))
        print " -- compute all scores --"
        for i, im in enumerate(self.integralImages):
            scores[i, :] = [hf.evaluate(im) for hf in self.haarFeatures]

        weights_pos = np.ones(len(self.posImages), dtype='float') * 1.0 / (
                           2*len(self.posImages))
        weights_neg = np.ones(len(self.negImages), dtype='float') * 1.0 / (
                           2*len(self.negImages))
        weights = np.hstack((weights_pos, weights_neg))

        print " -- select classifiers --"
        for _ in range(num_classifiers):
            # Viola Jones algorithm

            # Normalize the weights
            weights = weights / np.sum(weights)

            # Instantiate and train a classifier
            # Try changing polarity, but likely not high priority
            h_j = VJ_Classifier(scores, self.labels, weights)
            h_j.train()

            # Find epsilon_t
            epsilon_t = h_j.error

            # Append h_j to the list of classifiers
            self.classifiers.append(h_j)

            # Check to see if error is below threshold
            if epsilon_t == 0.0:
                alpha_t = np.log(1.0 / 1e-21)
                self.alphas.append(alpha_t)
                # print "ENDED EARLY AFTER {} ITERATIONS".format(
                #         len(self.classifiers))
                break
            else:
                # Update the weights
                beta_t = epsilon_t / (1.0 - epsilon_t)
                for i in range(len(weights)):
                    correct_prediction = h_j.predict(scores[i]) == self.labels[i]
                    e_i = -1 if correct_prediction else 1
                    weights[i] = weights[i] * np.power(beta_t, (1 - e_i))

                # Calculate and append alpha
                alpha_t = np.log(1.0 / beta_t)
                self.alphas.append(alpha_t)

        assert len(self.classifiers) == len(self.alphas)

    def predict(self, images):
        """Return predictions for a given list of images.

        Args:
            images (list of element of type numpy.array): list of images (observations).

        Returns:
            list: Predictions, one for each element in images.
        """
        ii = convert_images_to_integral_images(images)

        weighted_sums = np.zeros((len(self.classifiers), len(ii), 2)
                                  , dtype=np.float64)
        result = []

        # Populate the score location for each classifier 'clf' in
        # self.classifiers.
        for t, clf in enumerate(self.classifiers):
            scores = np.zeros((len(ii), len(self.haarFeatures))
                                , dtype=np.float64)
            image_score = np.zeros((len(ii), 2), dtype=np.float64)

            # Obtain the Haar feature id from clf.feature
            feature_id = clf.feature

            # Use this id to select the respective feature object from
            # self.haarFeatures
            best_feature = self.haarFeatures[feature_id]

            # Find the scores for every image using the best feature
            # Add the score value to scores[x, feature id] calling the
            # feature's evaluate function. 'x' is each image in 'ii'
            for x, i_image in enumerate(ii):
                score_value = best_feature.evaluate(i_image)
                scores[x, feature_id] = score_value
                # Append the results for each row in 'scores'. This value is
                # obtained using the equation for the strong classifier H(x).
                # Only one column is used per classifier
                h_t = clf.predict(scores[x])
                image_score[x, :] = np.asarray((h_t * self.alphas[t]
                                                        , self.alphas[t]))

            weighted_sums[t, :, :] = image_score

        for image_idx in range(len(ii)):
            class_sum, alpha_sum = np.sum(weighted_sums[:, image_idx, :]
                                          , axis=0)
            class_label = 1 if class_sum >= (0.5 * alpha_sum) else -1
            result.append(class_label)

        return result

    def faceDetection(self, image, filename):
        """Scans for faces in a given image.

        Complete this function following the instructions in the problem set
        document.

        Use this function to also save the output image.

        Args:
            image (numpy.array): Input image.
            filename (str): Output image file name.

        Returns:
            None.
        """
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        rows, cols = gray_image.shape

        sum_posi = 0.0
        sum_posj = 0.0
        faces_found = 0

        for posi in range(0, rows-24):
            for posj in range(0, cols-24):
                window = gray_image[posi:posi+24, posj:posj+24]
                assert window.shape == (24, 24)
                result = self.predict([window])
                if result[0] == 1:
                    # print "FOUND FACE: {} {}".format(posi, posj)
                    sum_posi += posi
                    sum_posj += posj
                    faces_found += 1

        avg_posi = int(sum_posi / faces_found)
        avg_posj = int(sum_posj / faces_found)
        # print "AvgPosI: {} AvgPosJ: {}".format(avg_posi, avg_posj)

        cv2.rectangle(image, (avg_posj, avg_posi), (avg_posj+24, avg_posi+24),
                       (0, 0, 255), 1)
        # cv2.imshow('rectangle', image)
        # cv2.waitKey(0)
        # cv2.rectangle(image, (49, 14), (73, 38), (0, 0, 255), 1)
        cv2.imwrite(os.path.join("output", filename), image)
